package com.ms3.wildcards.service;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.ms3.wildcards.conf.WildcardsConfiguration;
import com.ms3.wildcards.core.SFTPServer;
import com.ms3.wildcards.model.MS3Server;
import com.ms3.wildcards.util.CommonUtil;
import com.ms3.wildcards.util.SFTPUtil;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.inject.Inject;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 3/4/2016 7:06 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class SFTPService {

    @Inject
    private WildcardsConfiguration conf;

    public void copyAndMove( String fileName ){
        try{
            MS3Server origin = conf.getOriginServer().setFileName( fileName );
            MS3Server destination = conf.getDestinationServer().setFileName( fileName );

            SFTPServer originSftpServer = new SFTPServer( origin ).build();
            SFTPServer destinationSftpServer = new SFTPServer( destination ).build();

            originSftpServer.copyTo(destinationSftpServer);
            originSftpServer.copyTo(originSftpServer);

        }catch( Exception ex ){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public void move( String fileName ){
        try{
            MS3Server origin = conf.getOriginServer().setFileName( fileName );
            MS3Server destination = conf.getDestinationServer().setFileName( fileName );

            SFTPServer originSftpServer = new SFTPServer( origin ).build();
            SFTPServer destinationSftpServer = new SFTPServer( destination ).build();

            originSftpServer.moveTo(destinationSftpServer);
        }catch(Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public void copy( String fileName ){
        try{
            MS3Server origin = conf.getOriginServer().setFileName( fileName );
            MS3Server destination = conf.getDestinationServer().setFileName( fileName );

            SFTPServer originSftpServer = new SFTPServer(origin).build();
            SFTPServer destinationSftpServer = new SFTPServer(destination).build();

            originSftpServer.copyTo(destinationSftpServer);
        }catch(Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public String getFileName( String serverName, String path, String fileName ){
        ChannelSftp channelSftp = null;
        Session session = null;
        InputStream is = null;

        try{
            if( CommonUtil.isNull(fileName) ) {
                throw new Exception("File not found.");
            }

            if( CommonUtil.isNull(path) || !path.toLowerCase().matches("source|processed")){
                throw new Exception("Invalid file path.");
            }

            MS3Server server = getServer( serverName );
            session = SFTPUtil.constructSession( server.setFileName(fileName) );
            channelSftp = (ChannelSftp) SFTPUtil.constructChannel( session );

            String filePath = server.getProcessedFilePath();

            if( !CommonUtil.isNull(path) ){
                if (path.equals("source")) {
                    filePath = server.getSourceFilePath();
                }else if (path.equals("processed")) {
                    filePath = server.getProcessedFilePath();
                }
            }

            String absolutePath = String.format("%s/%s", filePath, server.getFileName());
            try{is = channelSftp.get(absolutePath);}catch(Exception e){;}
            if( CommonUtil.isNull(is)){
                return "File Not found.";
            }
            return server.getFileName();
        }catch (Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }finally{
            CommonUtil.closeQuitely(is);
            CommonUtil.closeQuitely(channelSftp);
            CommonUtil.closeQuitely(session);
        }
    }

    public List<ChannelSftp.LsEntry> getFiles( MS3Server server, String path ){
        ChannelSftp channelSftp = null;
        Session session = null;
        try{
            session = SFTPUtil.constructSession( server );
            channelSftp = (ChannelSftp) SFTPUtil.constructChannel( session );
            String filePath = server.getProcessedFilePath();

            if( !CommonUtil.isNull(path) ){
                if (path.equals("source")) {
                    filePath = server.getSourceFilePath();
                }else if (path.equals("processed")) {
                    filePath = server.getProcessedFilePath();
                }
            }

            return channelSftp.ls(filePath);
        }catch (Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }finally {
            CommonUtil.closeQuitely(channelSftp);
            CommonUtil.closeQuitely(session);
        }
    }

    private MS3Server getServer( String name ) throws Exception{
        if( CommonUtil.isNull(name) || !name.toLowerCase().matches("origin|destination")){
            throw new Exception("Server not found.");
        }
        return name.equalsIgnoreCase("origin") ? conf.getOriginServer() : conf.getDestinationServer();
    }

    public List<String> getFileNames( String serverName, String path ){
        try{
            if( CommonUtil.isNull(path) || !path.toLowerCase().matches("source|processed")){
                throw new Exception("File path not found.");
            }

            MS3Server server = getServer( serverName );

            List<ChannelSftp.LsEntry> list = getFiles(server, path);
            List<String> names = new ArrayList<String>();
            for( ChannelSftp.LsEntry entry : list ){
                if( !entry.getAttrs().isDir() ) {
                    names.add( entry.getFilename());
                }
            }

            return names;
        }catch (Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public String upload( File file, FormDataContentDisposition fileDetail, String serverName){
        try{
            String fileName = fileDetail.getFileName();
            MS3Server server = getServer( serverName ).setFileName( fileName );
            SFTPUtil.upload( file, server );

            return String.format("uploaded %s to %s", fileDetail.getFileName(), server.getSourceFilePath());
        }catch (Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

}
