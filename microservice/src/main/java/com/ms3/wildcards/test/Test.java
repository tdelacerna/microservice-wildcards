package com.ms3.wildcards.test;

//import org.apache.ftpserver.FtpServerFactory;
//import org.apache.ftpserver.listener.ListenerFactory;

import java.util.List;

import com.jcraft.jsch.*;
import com.ms3.wildcards.util.CommonUtil;

/**
 * 2/24/2016 8:46 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class Test {

    public static void main(String[] args){
        String host = "52.73.159.185";
        int    port = 22;
        String user = "ec2-user";
        String passphrase = null;
        String remoteFilePath = "local/source";
        String prikeyfile = "ms3-ProdKey.ppk";

        Session     session     = null;
        Channel     channel     = null;
        ChannelSftp channelSftp = null;

        System.out.println("~~~~~");
        try {
            System.out.println("++++++++++++++");
            JSch jsch = new JSch();
            jsch.addIdentity(prikeyfile);
//            jsch.addIdentity(prikeyfile, passphrase);
            session = jsch.getSession(user,host,port);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            System.out.println(channel.isConnected());
            channelSftp = (ChannelSftp)channel;
//            channelSftp.connect();
//            channelSftp.cd(remoteFilePath);

            List<ChannelSftp.LsEntry> list = channelSftp.ls(remoteFilePath);
            for( ChannelSftp.LsEntry entry : list ){
                if( !entry.getAttrs().isDir() ) {
                    System.out.println("==> " + entry.getFilename());
                }
            }
            System.out.println("++++++++++++++");
        } catch(JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }finally{
            CommonUtil.closeQuitely(session);
            CommonUtil.closeQuitely(channel);
            CommonUtil.closeQuitely(channelSftp);
        }

        System.out.println("~~~~~");

    }

//
//    public static void main1(String[] args){
//
//        FtpServerFactory serverFactory = new FtpServerFactory();
//        ListenerFactory factory = new ListenerFactory();
//
//    }
//
//    public static void main( String[] args ){
//        try{
//            String fileToDownload = "";
//
//            downloadFile( fileToDownload );
//
//        }catch(Exception ex ){
//            ex.printStackTrace();
//        }
//
//
//
//    }
//
//    public static boolean downloadFile(String fileToDownload){
//
//        Properties props = new Properties();
//        StandardFileSystemManager manager = new StandardFileSystemManager();
//
//        try {
//
//            String serverAddress = props.getProperty("serverAddress").trim();
//            String userId = props.getProperty("userId").trim();
//            String password = props.getProperty("password").trim();
//            String remoteDirectory = props.getProperty("remoteDirectory").trim();
//            String localDirectory = "C:\\DEV\\";
//
//
//            //Initializes the file manager
//            manager.init();
//
//            //Setup our SFTP configuration
//            FileSystemOptions opts = new FileSystemOptions();
//            SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");
//            SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
//            SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
//
//            //Create the SFTP URI using the host name, userid, password,  remote path and file name
////            String sftpUri = "sftp://" + userId + ":" + password +  "@" + serverAddress + "/" +
////                    remoteDirectory + fileToDownload;
//            String sftpUri = String.format("sftp://%s:%s@%s/%s%s", userId, password, serverAddress, remoteDirectory, fileToDownload);
//            // Create local file object
//            String filepath = String.format("%s%s",localDirectory,fileToDownload);
//            File file = new File(filepath);
//            FileObject localFile = manager.resolveFile(file.getAbsolutePath());
//
//            // Create remote file object
//            FileObject remoteFile = manager.resolveFile(sftpUri, opts);
//
//            // Copy local file to sftp server
//            localFile.copyFrom(remoteFile, Selectors.SELECT_SELF);
//
//            System.out.println("File download successful");
//
//        }
//        catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        finally {
//            manager.close();
//        }
//
//        return true;
//    }

}

