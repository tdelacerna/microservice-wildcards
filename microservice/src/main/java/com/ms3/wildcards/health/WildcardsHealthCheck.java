package com.ms3.wildcards.health;

import com.codahale.metrics.health.HealthCheck;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.ms3.wildcards.conf.WildcardsConfiguration;
import com.ms3.wildcards.model.MS3Server;
import com.ms3.wildcards.util.CommonUtil;
import com.ms3.wildcards.util.SFTPUtil;
import io.dropwizard.Configuration;

/**
 * 2/18/2016 10:37 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class WildcardsHealthCheck extends HealthCheck {

    private final WildcardsConfiguration conf;

    public WildcardsHealthCheck( Configuration conf){
        this.conf = (WildcardsConfiguration) conf;
    }

    @Override
    protected Result check() throws Exception{
        try{ validate( conf.getOriginServer() ) ; }catch (Exception ex){ return Result.unhealthy(ex.getMessage()); }
        try{ validate( conf.getDestinationServer() ) ; }catch (Exception ex){ return Result.unhealthy(ex.getMessage()); }
        
        return Result.healthy();
    }

    public void validate( MS3Server server ) throws Exception{
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        try {
            session = SFTPUtil.constructSession(server);
            channel = SFTPUtil.constructChannel(session);
            channelSftp = (ChannelSftp) channel;
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }finally {
            CommonUtil.closeQuitely( session );
            CommonUtil.closeQuitely( channel );
            CommonUtil.closeQuitely( channelSftp );
        }
    }

}
