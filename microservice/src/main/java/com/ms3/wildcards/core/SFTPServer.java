package com.ms3.wildcards.core;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.ms3.wildcards.model.MS3Server;
import com.ms3.wildcards.util.CommonUtil;
import com.ms3.wildcards.util.SFTPUtil;
import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.Selectors;
import org.omg.CORBA.COMM_FAILURE;

import java.io.*;

/**
 * 3/3/2016 9:29 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class SFTPServer {

    private MS3Server server;
    private static SFTPServer instance;

    private Session session = null;
    private Channel channel = null;
    private ChannelSftp channelSftp = null;

    public SFTPServer(){}

    public SFTPServer(MS3Server server) throws  Exception{
        if(CommonUtil.isNull( server )){
            throw new Exception("MS3Server cannot be null!");
        }

        this.server = server;
    }

    public static SFTPServer getInstance(){
        if( CommonUtil.isNull(instance) ){
            instance = new SFTPServer();
        }
        return instance;
    }

    public SFTPServer build( MS3Server server ){
        try{
            if(CommonUtil.isNull( server )){
                throw new Exception("MS3Server cannot be null!");
            }
            this.server = server;
            this.session = SFTPUtil.constructSession(getServer());
            this.channel = SFTPUtil.constructChannel(getSession());
            this.channelSftp = (ChannelSftp) channel;

            return this;
        } catch (Exception ex) {
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public SFTPServer build(){
        try{
            this.session = SFTPUtil.constructSession(getServer());
            this.channel = SFTPUtil.constructChannel(getSession());
            this.channelSftp = (ChannelSftp) channel;

            return this;
        } catch (Exception ex) {
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public MS3Server getServer() {
        return server;
    }

    public Session getSession() {
        return session;
    }

    public Channel getChannel() {
        return channel;
    }

    public ChannelSftp getChannelSftp(){
        return channelSftp;
    }

    public SFTPServer copyTo( SFTPServer destination ) {
        InputStream is = null;
        try{
            if( this.equals(destination)){
                destination = new SFTPServer( destination.getServer() ).build();
            }

            ChannelSftp channel = destination.getChannelSftp();
            String originSourceFile = String.format("%s/%s", getServer().getSourceFilePath(), getServer().getFileName());
            String destinationProcessedFile = String.format("%s/%s", destination.getServer().getProcessedFilePath(), destination.getServer().getFileName());
            is = getChannelSftp().get(originSourceFile);

            channel.put(is, destinationProcessedFile, ChannelSftp.OVERWRITE);

            return this;
        } catch (Exception ex) {
            throw new IllegalStateException(ex.getMessage(), ex);
        }finally{
            CommonUtil.closeQuitely( is );
            destination.destroy();
        }
    }

    public SFTPServer moveTo( SFTPServer destination ){
        try{
            this.copyTo(destination).delete();

            return this;
        } catch (Exception ex) {
            throw new IllegalStateException(ex.getMessage(), ex);
        }finally{
            destination.destroy();
        }
    }

    public void destroy(){
        CommonUtil.closeQuitely( getSession() );
        CommonUtil.closeQuitely( getChannel() );
        CommonUtil.closeQuitely( getChannelSftp() );
    }

    public SFTPServer delete() throws Exception{
        String sourceFile = String.format("%s/%s", getServer().getSourceFilePath(), getServer().getFileName());
        getChannelSftp().rm( sourceFile );

        return this;
    }
}
