package com.ms3.wildcards;

import com.ms3.wildcards.binder.DependencyBinder;
import com.ms3.wildcards.conf.WildcardsConfiguration;
import com.ms3.wildcards.health.WildcardsHealthCheck;
import com.ms3.wildcards.resource.WildcardsResource;
import com.ms3.wildcards.service.SFTPService;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 * 2/18/2016 10:40 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class WildcardsApplication extends Application<WildcardsConfiguration> {

    public static void main(String[] args) throws Exception{
        new WildcardsApplication().run(args);
    }

    @Override
    public String getName(){ return "micro-service"; }

    @Override
    public void initialize(Bootstrap<WildcardsConfiguration> bootstrap){}

    @Override
    public void run(WildcardsConfiguration conf, Environment environment ){
        final WildcardsResource wildcardsResource = new WildcardsResource();
        final WildcardsHealthCheck healthCheck = new WildcardsHealthCheck( conf );

        environment.healthChecks().register("configuration",healthCheck);

        environment.jersey().register( wildcardsResource );
        environment.jersey().register( MultiPartFeature.class );
        environment.jersey().register( new DependencyBinder( conf ) );
    }

}
