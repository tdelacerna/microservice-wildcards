package com.ms3.wildcards.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import org.apache.commons.vfs.impl.StandardFileSystemManager;

/**
 * Created by Teodoro Dela Cerna on 3/11/2016.
 */
public class CommonUtil {

    public static void closeQuitely(AutoCloseable resource) {
        if (null != resource) {
            try { resource.close(); } catch (Exception ex) {; }
        }
    }

    public static void closeQuitely(Session session) {
        if (null != session) {
            try {session.disconnect();} catch (Exception ex) {ex.printStackTrace();}
        }
    }

    public static void closeQuitely(Channel channel) {
        if (null != channel) {
            try {channel.disconnect();} catch (Exception ex) {;}
        }
    }

    public static void closeQuitely(ChannelSftp channel) {
        if (null != channel) {
            try {channel.disconnect();} catch (Exception ex) {ex.printStackTrace();}
        }
    }

    public static void closeQuitely(StandardFileSystemManager manager) {
        if (null != manager) {
            try {manager.close();} catch (Exception ex) {;}
        }
    }

    public static boolean toBoolean( Object obj ){
        return false;
    }

    public static boolean isNull(Object... object) {
        for (Object obj : object) {
            if( obj instanceof String ){
                String str = (String)obj;
                return str == null || str.isEmpty() ? true : false;
            }

            return (null == obj);
        }

        return true;
    }

}
