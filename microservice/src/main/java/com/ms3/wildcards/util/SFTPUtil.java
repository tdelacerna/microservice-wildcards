package com.ms3.wildcards.util;

import com.jcraft.jsch.*;
import com.ms3.wildcards.model.MS3Server;
import org.apache.commons.vfs.*;
import org.apache.commons.vfs.impl.StandardFileSystemManager;
import org.apache.commons.vfs.provider.sftp.SftpFileSystemConfigBuilder;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.logging.Logger;

/**
 * 2/24/2016 10:53 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class SFTPUtil {

    private static final Logger logger = Logger.getLogger(SFTPUtil.class.getName());

    public static String constructConnStr(MS3Server server){
        String hostName = server.getPublicIp();
        String port = server.getPort();
        String userName  = server.getUserName();
        String password  = server.getPassword();
        String fileName = server.getFileName();
        String key = server.getPrivateKey();

        String filePath = server.isForCopy() ? server.getProcessedFilePath() : server.getSourceFilePath();
        if( key != null && !key.isEmpty() ){
            return String.format("sftp://%s@%s:%s/%s/%s", userName, hostName, port, filePath, fileName);
        }else{
            return String.format("sftp://%s:%s@%s:%s/%s/%s", userName, password, hostName, port, filePath, fileName);
        }
    }

    public static Session constructSession( MS3Server server ){
        try{
            String host = server.getHost();
            int port = Integer.parseInt(server.getPort());
            String userName = server.getUserName();
            String password = server.getPassword();
            String passPhrase = server.getPassPhrase();
            String key = server.getPrivateKey();

            JSch jsch = new JSch();
            if( !CommonUtil.isNull( key ) ){
                jsch.addIdentity(key, passPhrase);
            }

            Session session = jsch.getSession(userName,host,port);
            if( CommonUtil.isNull( key ) ){
                session.setPassword( password );
            }

            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            return session;
        }catch(Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public static Channel constructChannel( Session session ){
        return constructChannel( session, "sftp");
    }

    public static Channel constructChannel( Session session, String type ){
        try {
            Channel channel = session.openChannel( CommonUtil.isNull(type) ? "sftp" : type );
            channel.connect();

            return channel;
        } catch (JSchException ex) {
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    public static FileObject constructFileObject( MS3Server server ){
        StandardFileSystemManager manager = new StandardFileSystemManager();
        try{
            manager.init();
            String connStr = String.format("%s", constructConnStr(server));
            FileSystemOptions fileOptions = createDefaultOptions( server );

            return manager.resolveFile(connStr, fileOptions);
        }catch( Exception ex ){
            throw new IllegalStateException(ex.getMessage(),ex);
        }finally{
            CommonUtil.closeQuitely(manager);
        }
    }

    public static void upload( File file, MS3Server server ) {
        if ( file == null || !file.exists())
            throw new RuntimeException("Error. Local file not found");

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        InputStream is = null;

        try{
            String absoluteFilePath = String.format("%s/%s", server.getUploadsFilePath(), server.getFileName());
            session = constructSession( server );
            channel = constructChannel( session );
            channelSftp = (ChannelSftp)channel;

            channelSftp.put(new FileInputStream(file), absoluteFilePath, ChannelSftp.OVERWRITE);
        }catch (Exception ex ){
            throw new IllegalStateException(ex.getMessage(), ex);
        }finally{
            CommonUtil.closeQuitely(is);
            CommonUtil.closeQuitely(session);
            CommonUtil.closeQuitely(channel);
            CommonUtil.closeQuitely(channelSftp);
        }

    }

    private static FileSystemOptions createDefaultOptions( MS3Server server ) throws FileSystemException {
        // Create SFTP options
        FileSystemOptions opts = new FileSystemOptions();

        // SSH Key checking
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");

        // Root directory set to user home
        SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);

        // Timeout is count by Milliseconds
        SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
        String key = server.getPrivateKey();
        if( key != null){
            SftpFileSystemConfigBuilder.getInstance().setUserInfo(opts, new SftpPassphraseUserInfo(null));
            SftpFileSystemConfigBuilder.getInstance().setIdentities(opts, new File[] { new File(key) });
        }

        return opts;
    }

    public static class SftpPassphraseUserInfo implements UserInfo {

        private String passphrase = null;

        public SftpPassphraseUserInfo(final String pp) {
            passphrase = pp;
        }

        public String getPassphrase() {
            return passphrase;
        }

        public String getPassword() {
            return null;
        }

        public boolean promptPassphrase(String arg0) {
            return true;
        }

        public boolean promptPassword(String arg0) {
            return false;
        }

        public void showMessage(String message) {

        }

        public boolean promptYesNo(String str) {
            return true;
        }

    }


}
