package com.ms3.wildcards.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 2/24/2016 11:20 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class MS3Server {

    @NotEmpty
    @JsonProperty
    private String host;

    @NotEmpty
    @JsonProperty
    private String port;

    @NotEmpty
    @JsonProperty
    private String publicIp;

    @NotEmpty
    @JsonProperty
    private String privateIp;

    @NotEmpty
    @JsonProperty
    private String userName;

    @NotEmpty
    @JsonProperty
    private String password;

    @NotEmpty
    @JsonProperty
    private String passPhrase;

    @NotEmpty
    @JsonProperty
    private String uploadsFilePath;

    @NotEmpty
    @JsonProperty
    private String sourceFilePath;

    @NotEmpty
    @JsonProperty
    private String processedFilePath;

    @JsonProperty
    private String privateKey;

    private String fileName;

    private boolean isForCopy;

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getPublicIp() {
        return publicIp;
    }

    public String getPrivateIp() {
        return privateIp;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getPassPhrase() {
        return passPhrase;
    }

    public String getUploadsFilePath() {
        return uploadsFilePath;
    }

    public String getSourceFilePath() {
        return sourceFilePath;
    }

    public String getProcessedFilePath() {
        return processedFilePath;
    }

    public MS3Server setFileName(String fileName){
        this.fileName = fileName;
        return this;
    }

    public String getFileName(){ return fileName; }

    public String getPrivateKey() {
        return privateKey;
    }

    public MS3Server setForCopy(boolean isForCopy){
        this.isForCopy = isForCopy;
        return this;
    }

    public boolean isForCopy(){ return isForCopy; }

}
