package com.ms3.wildcards.resource;

import com.ms3.wildcards.service.SFTPService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.List;
import com.google.common.base.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.glassfish.jersey.media.multipart.FormDataParam;
import javax.inject.Inject;

/**
 * 2/18/2016 10:49 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
@Path("/file")
@Produces(MediaType.APPLICATION_JSON)
public class WildcardsResource {

    @Inject
    SFTPService service;

    @GET
    @Path("/get-file-names")
    @Produces(MediaType.APPLICATION_JSON)
    public List getFileNames(@QueryParam("server") Optional<String> server,
                             @QueryParam("path") Optional<String> path) {
        try{

            return service.getFileNames(server.or("origin"), path.or("processed"));
        }catch (Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex );
        }
    }

    @GET
    @Path("/get-file-name")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFileName(@QueryParam("server") Optional<String> serverName,
                              @QueryParam("path") Optional<String> path,
                              @QueryParam("fileName") String fileName) {
        try{

            return service.getFileName(serverName.or("origin"), path.or("processed"), fileName );
        }catch (Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex );
        }
    }

    @POST
    @Path("/copy-move")
    @Produces(MediaType.APPLICATION_JSON)
    public Response copyAndMove(@QueryParam("server") Optional<String> server,
                                @QueryParam("fileName") String fileName ) {
        try{
            service.copyAndMove( fileName );

            return Response.ok( "successfully copied and moved file." ).build();
        }catch(Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    @POST
    @Path("/move")
    @Produces(MediaType.APPLICATION_JSON)
    public Response move( @QueryParam("server") Optional<String> server,
                          @QueryParam("fileName") String fileName ){
        try{
            service.move( fileName );

            return Response.ok( "successfully moved file." ).build();
        }catch(Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    @POST
    @Path("/copy")
    @Produces(MediaType.APPLICATION_JSON)
    public Response copy( @QueryParam("server") Optional<String> server,
                          @QueryParam("fileName") String fileName ){
        try{

            service.copy( fileName );

            return Response.ok( "Successfully copied file." ).build();
        }catch(Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(@FormDataParam("file") File file,
                           @FormDataParam("file") FormDataContentDisposition fileDetail,
                           @QueryParam("server") Optional<String> server) throws IOException {

        String response = service.upload( file, fileDetail, server.or("origin") );

        return Response.ok( response ).build();
    }

}
