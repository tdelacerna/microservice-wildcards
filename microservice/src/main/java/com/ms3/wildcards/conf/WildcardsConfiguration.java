package com.ms3.wildcards.conf;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ms3.wildcards.model.MS3Server;
import io.dropwizard.Configuration;

import javax.validation.constraints.NotNull;

/**
 * 2/18/2016 10:34 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class WildcardsConfiguration extends Configuration{

    @NotNull
    @JsonProperty
    private MS3Server originServer;

    @NotNull
    @JsonProperty
    private MS3Server destinationServer;

    public MS3Server getOriginServer() {
        return originServer;
    }

    public MS3Server getDestinationServer() {
        return destinationServer;
    }

}
