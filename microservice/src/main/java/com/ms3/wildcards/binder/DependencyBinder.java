package com.ms3.wildcards.binder;

import com.google.common.reflect.ClassPath;
import com.ms3.wildcards.conf.WildcardsConfiguration;
import com.ms3.wildcards.service.SFTPService;
import io.dropwizard.Configuration;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 * 3/7/2016 7:17 PM
 *
 * @author � tdelacerna <tdelacerna@ms3-inc.com>
 */
public class DependencyBinder extends AbstractBinder {

    private WildcardsConfiguration conf;

    public DependencyBinder( Configuration conf ){
        this.conf = (WildcardsConfiguration) conf;
    }

    @Override
    protected void configure() {
        bind(this.conf).to(WildcardsConfiguration.class);
        bind(SFTPService.class).to(SFTPService.class);
    }

    protected void configure1() {
        try{
            final ClassLoader loader = Thread.currentThread().getContextClassLoader();

            for (final ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClasses()) {
                if (info.getName().startsWith("com.ms3.wildcards.service")) {
                    final Class<?> c = info.load();

//                    bind(c.class).to(c.class);

                }
            }
        }catch (Exception ex){
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

}
